import * as ActionTypes from '../ActionTypes';

const initialState = {
    user: {
        username: '',
    },

};


export const Reducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.ADD_USER:
            return {
                ...state, user: { ...state.user, username: action.payload.username },
            };

        case ActionTypes.REMOVE_USER:
            return {
               
                ...state, user: { ...state.user, username: action.payload.username =='' },
               
            };


        default:
            return state;
    }

};