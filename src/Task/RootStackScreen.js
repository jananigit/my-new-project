import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Welcome from './Welcome';
import Login from './Login';
import Signup from './Signup';
import Home from './Home'

const RootStack = createNativeStackNavigator();

const RootStackScreen = ({ navigation }) => (
    <RootStack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Welcome">

        <RootStack.Screen name="Welcome" component={Welcome} />
        <RootStack.Screen name="Signup" component={Signup} />
        <RootStack.Screen name="Login" component={Login} />
        <RootStack.Screen name="Home" component={Home} />



    </RootStack.Navigator>
);

export default RootStackScreen;