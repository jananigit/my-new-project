import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, TextInput } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import * as ActionTypes from '../Redux/ActionTypes';

const Home = (props) => {

    const [name, setName] = useState('');


    const handleInput = () => {
        return (
            props.addUser(name),
            setName({ name: '', })
        );
    }
    const removeInput = () => props.removeUser(name);

    return (
        <View style={styles.container} >
            <View style={{ marginTop: 170 }}>
                <Text style={{ fontSize: 22, color: 'black' }}>Enter Name</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={(text) => setName(text)}
                    value={name === '' ? null : name}
                   
                />
            </View>

            <View>
                <TouchableOpacity onPress={() => handleInput()} style={[styles.button, { marginVertical: 20 }]}>
                    <Text style={{ fontSize: 20, color: 'black' }}>Submit</Text>
                </TouchableOpacity>
            </View>
            <View style={{ marginTop: 100 }}>
                <Text style={{ fontSize: 22, color: 'black' }}>Output</Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.textInput}>{props.user.username}</Text>
                    <TouchableOpacity onPress={() => removeInput()}>
                        <MaterialCommunityIcons name="close-circle" size={28} color='black' style={{ marginTop: 22, marginLeft: 10 }} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => ({ user: state.user });

const mapDispatchToProps = (dispatch) => ({
    addUser: (username) =>
        dispatch({
            type: ActionTypes.ADD_USER,
            payload: {
                username,
            }
        }),
    removeUser: (username) =>
        dispatch({
            type: ActionTypes.REMOVE_USER,
            payload: {
                username,
            }
        }) 
})


export default connect(mapStateToProps, mapDispatchToProps)(Home);



const styles = StyleSheet.create({
    container: {
        flex: 1,

        alignItems: 'center',
        backgroundColor: '#fff'
    },

    textInput: {
        marginTop: 15,
        width: 280,
        padding: 12,
        borderWidth: 1,
        marginVertical: 20

    },
    button: {
        borderWidth: 1,

        borderRadius: 5,
        width: 200,
        height: 50,
        textAlign: 'center',
        padding: 10,
        justifyContent: 'center',
        fontSize: 16,
        alignItems: 'center',




    },


});