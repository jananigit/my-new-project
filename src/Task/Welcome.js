import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, SafeAreaView } from 'react-native';




const Welcome = ({ navigation }) => {





    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1 }}>
                <Image
                    style={{ height: 350, width: 350, marginTop: 30 }}
                    source={require('../../assests/FirstScreen.png')}
                />


            </View>
            <View style={styles.txtContainer}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' }}>LEARN ON THE GO</Text>
                <Text style={{ fontSize: 15, marginTop: 20, textAlign: 'center' }}>Master your skills with fun and learn from very fundamentals</Text>

                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
                        <Text style={[styles.button, { color: 'white', backgroundColor: 'dodgerblue' }]}>Sign up</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                        <Text style={[styles.button, { color: 'dodgerblue', }]}>Sign in</Text>
                    </TouchableOpacity>
                </View>
            </View>





        </SafeAreaView>
    );
}

export default Welcome;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    txtContainer: {

        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 50,
        marginHorizontal: 50
    },
    button: {
        borderWidth: 1,
        borderColor: 'dodgerblue',
        borderRadius: 15,
        width: 150,
        height: 50,
        textAlign: 'center',
        padding: 12,
        fontWeight: '600',
        fontSize: 16,
        marginHorizontal: 10,
        marginTop: 130,
    },


});