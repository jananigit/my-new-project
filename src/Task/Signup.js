import React from 'react';
import { StyleSheet, Text, TextInput, View, SafeAreaView, TouchableOpacity, Image, Alert } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


import { AuthenticationStyles } from '../Styles/GlobalStyles';


const Signup = ({ navigation }) => {


    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')

    const validation = () => {

        if (email === '') {
            Alert.alert('Email field cannot be Empty!')
        } else if (password === '') {
            Alert.alert('Password field cannot be Empty!')
        } else {

            navigation.navigate('Home');
        }
    }



    const Styles = AuthenticationStyles();

    return (
        <SafeAreaView style={Styles.container}>
            <KeyboardAwareScrollView>
                <AntDesign name="arrowleft" size={25} color='dodgerblue' style={{ margin: 20, }} onPress={() => { navigation.goBack() }} />
                <View style={{ margin: 30 }}>
                    <Text style={{ fontSize: 28, fontWeight: 'bold', color: 'black', marginTop: -20 }}>Create </Text>
                    <Text style={{ fontSize: 28, fontWeight: 'bold', color: 'black', marginTop: 4 }}>Account </Text>
                    <Text style={{ marginTop: 50, color: 'black' }}>Your Email</Text>
                    <TextInput

                        style={styles.textInput}
                        autoCapitalize='none'
                        keyboardType='email-address'
                        onChangeText={(text) => setEmail(text)}
                    />
                    <Text style={{ marginTop: 15, color: 'black' }}>Password</Text>
                    <TextInput

                        style={styles.textInput}
                        autoCapitalize='none'
                        secureTextEntry={true}
                        onChangeText={(text) => setPassword(text)}
                    />
                </View>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={validation} activeOpacity={0.4}>
                        <Text style={[styles.button, { marginTop: 40, fontSize: 18, color: 'white', backgroundColor: 'dodgerblue' }]}>Create account</Text>
                    </TouchableOpacity>

                    <View style={[styles.button, { marginTop: 30, fontSize: 16, color: 'dodgerblue', backgroundColor: 'white' }]}>
                        <TouchableOpacity style={{ flexDirection: 'row' }}>

                            <Image
                                style={{ width: 30, height: 30, marginTop: 6, marginHorizontal: 10 }}
                                source={require('../../assests/google.png')}
                            />
                            <Text style={{ fontSize: 17, marginTop: 8, color: 'dodgerblue', marginHorizontal: 10 }}>Sign up with Google</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ marginTop: 45 }}>
                        <Text style={{ margin: 15 }}>Already have an account ? </Text>
                        <Text onPress={() => navigation.navigate('Login')} style={{ fontWeight: '700', color: 'dodgerblue' }}>  Sign in</Text>
                    </Text>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>


    );
};

export default Signup;




const styles = StyleSheet.create({

    button: {
        borderWidth: 1,
        borderColor: 'dodgerblue',
        borderRadius: 15,
        width: 280,
        height: 50,
        textAlign: 'center',
        padding: 12,
        fontWeight: '600',

        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 80,
    },

    textInput: {
        marginTop: 15,
        width: 280,
        padding: 12,
        borderRadius: 10,
        backgroundColor: '#E3F1FA'
    },


});