import React from 'react'; 
import { NavigationContainer } from '@react-navigation/native';

import { Provider } from 'react-redux';
import {Store} from './src/Redux/Store';



import RootStackScreen from './src/Task/RootStackScreen';

const App = () => {

    return(
        <Provider store={Store}>
        <NavigationContainer>
            <RootStackScreen />
        
        </NavigationContainer>
        </Provider>

        
    );
};


export default App;